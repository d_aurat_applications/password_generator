# This script is here to help to create an installer.

VERSION_MAJOR=`grep "VERSION_MAJOR" sources/version.h | cut -d\  -f 3`
VERSION_MINOR=`grep "VERSION_MINOR" sources/version.h | cut -d\  -f 3`

pushd bin &> /dev/null

if [ ! -d program ]
then
	echo "Please create a folder named program which contains the executable and all needed dlls."
	exit 1
fi

# Create and fill the configuration directory.
mkdir config
pushd config &>/dev/null
# Create configuration file.
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Installer>
    <Name>Password generator</Name>
    <Version>${VERSION_MAJOR}.${VERSION_MINOR}</Version>
    <Title>Password generator Installer</Title>
    <Publisher>D. Aurat</Publisher>
    <StartMenuDir>Password generator</StartMenuDir>
    <TargetDir>@ApplicationsDirX86@/Password generator</TargetDir>
</Installer>" > config.xml
popd &>/dev/null

# Create and fill the packages directory.
mkdir packages
pushd packages &>/dev/null

# package com.vendor.product
mkdir com.vendor.product
pushd com.vendor.product &>/dev/null

#folder data
mkdir data
cp -r ../../program ./data/

#folder meta
mkdir meta
pushd meta &>/dev/null
CURRENT_DATE=`date +%Y-%m-%d`
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Package>
    <DisplayName>Password generator</DisplayName>
    <Description>Install Password generator.</Description>
    <Version>${VERSION_MAJOR}.${VERSION_MINOR}</Version>
    <ReleaseDate>${CURRENT_DATE}</ReleaseDate>
    <Default>script</Default>
	<Script>installscript.qs</Script>
	<RequiresAdminRights>true</RequiresAdminRights>
	<ForcedInstallation>true</ForcedInstallation>
</Package>" > package.xml

echo "
function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    try {
        // call the base create operations function
        component.createOperations();
	    if (systemInfo.productType === \"windows\") {
			component.addOperation(\"CreateShortcut\", \"@TargetDir@/program/password_generator.exe\", \"@DesktopDir@/Password generator.lnk\");
		}
    } catch (e) {
        print(e);
    }
}" > installscript.qs
popd &>/dev/null

popd &>/dev/null

popd &>/dev/null

/d/Qt/Tools/QtInstallerFramework/3.0/bin/binarycreator.exe -c config/config.xml -p packages PasswordGeneratorInstaller_v${VERSION_MAJOR}_${VERSION_MINOR}.exe

# Clean directories
rm -rf config
rm -rf packages

popd &>/dev/null