#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QMap>
#include <QStringList>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    /**
     * @brief Add all possible characters to the internal list.
     */
    void addPreselectedCharacters(const QString& charactersList);

private slots:
    /**
      * \brief Generate a new password and display it in the lineedit.
      * \details Activated when the button Generate is clicked.
      */
    void on_GenerateButton_clicked();

    /**
     * \brief Copy the password to the clipboard.
     * \details Activated when the button "Copy to clipboard" is clicked.
     */
    void on_CopyToClipboardButton_clicked();

private:
    //! User interface.
    Ui::MainWindow *ui;

    //! All preselected characters list.
    QMap<QCheckBox*, QLabel*> mPreselectedCharacters;

    //! Aditionnal characters requested by the user.
    QLineEdit* mAdditionnalCharacters;
};

#endif // MAIN_WINDOW_H
