#include "main_window.h"
#include "ui_main_window.h"

#include <QClipboard>
#include <QRandomGenerator>

#include "version.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    const auto windowTitle = QString("Password generator v%1.%2").arg(VERSION_MAJOR).arg(VERSION_MINOR);
    setWindowTitle(windowTitle);


    // Add "a->z" as preselected characters.
    addPreselectedCharacters("abcdefghijklmnopqrstuvwxyz");

    // Add "A->Z" as preselected characters.
    addPreselectedCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    // Add "0->9" as preselected characters.
    addPreselectedCharacters("01234567893");

    // Add "#_@$%*!:;,.<>+-|" as preselected characters.
    addPreselectedCharacters("#_@$%*!:;,.<>+-|");

    // Add user entry for specific characters.
    {
        const auto additionnalCharactersLabel = new QLabel("Additionnal characters");
        mAdditionnalCharacters = new QLineEdit;
        ui->allPossibleCharactersLayout->addRow(additionnalCharactersLabel, mAdditionnalCharacters);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mAdditionnalCharacters;
    qDeleteAll(mPreselectedCharacters);
    mPreselectedCharacters.clear();
}

void MainWindow::addPreselectedCharacters(const QString& charactersList)
{
    auto checkbox = new QCheckBox;
    auto charactersListLabel = new QLabel(charactersList);
    charactersListLabel->setAlignment(Qt::AlignLeft);
    mPreselectedCharacters[checkbox] = charactersListLabel;
    ui->allPossibleCharactersLayout->addRow(checkbox, charactersListLabel);
}

void MainWindow::on_GenerateButton_clicked()
{
    // 1. First, gather all possible characters.
    QString allAvailableCharacters;
    {
        // 1.1. Add preselected characters lists.
        auto checkboxAndLabel = mPreselectedCharacters.constBegin();
        while(checkboxAndLabel != mPreselectedCharacters.constEnd())
        {
            // Check if the user want to take this line.
            if(checkboxAndLabel.key()->isChecked())
            {
                // We take this line as available for all characters.
                allAvailableCharacters.append(checkboxAndLabel.value()->text());
            }
            ++checkboxAndLabel;
        }

        // 1.2. Add custom characters entry.
        const auto otherCharacters = mAdditionnalCharacters->text();
        allAvailableCharacters.append(otherCharacters);
    }

    // 2. Generate a new password.
    const auto nbPossibleChar = allAvailableCharacters.size();
    QString generatedPassword;
    auto randomGenerator = QRandomGenerator64::securelySeeded();
    for(auto ithChar = 0; ithChar < ui->PasswordLengthSpinBox->value(); ithChar++)
    {
        const auto choosenChar = randomGenerator.bounded(0, nbPossibleChar);
        generatedPassword += allAvailableCharacters[choosenChar];
    }

    // 3. Display it in the lineedit.
    ui->GeneratedPasswordLineEdit->setText(generatedPassword);
}

void MainWindow::on_CopyToClipboardButton_clicked()
{
    auto clipboard = QApplication::clipboard();
    clipboard->setText(ui->GeneratedPasswordLineEdit->text());
}
